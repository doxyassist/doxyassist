
#  DoxyAssist - making modular Doxygen documentation.
#  Copyright (C) 2010-2012 Wouter Haffmans (cheetah@simply-life.net)
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
@file
This module is used to invoke Doxygen, given a Doxyfile. It can also be used
to create a template Doxygen file.
"""

from . import doxyfile
import subprocess
from os import makedirs
from os import path

doxygen = '/usr/bin/doxygen'
""" The path to Doxygen; change this to match the Doxygen executable """

outputs = {
    'html': [],
    'docset': [],
    'chm': [],
    'chi': [],
    'qhp': [],
    'qch': [],
    'eclipse': [],
    'latex': [],
    'rtf': [],
    'man': [],
    'xml': [],
    'returncode': []
}
"""
Collected outputs of various runs.
After each run, the settings are inspected on the Doxyfile. If e.g. a 'html'
output is given, then the path to index.html file (e.g. output/html/index.html)#
is added to this list. Doxygen does verify that this file exists before returning
it. Note that run() additionally returns the outputs of that specific run as well.
The files that will be referred to (using "project.ext" as base filename, if
there is a Doxygen option to set the filename):

html: html/index.html
docset: html/Info.plist -- NOT TESTED
chm: html/project.chm -- NOT TESTED
chi: html/index.chi -- NOT TESTED
qhp: html/index.qhp
qch: html/project.qch
eclipse: html/plugin.xml
latex: latex/refman.tex
rtf: rtf/refman.rtf
man: man/man3/
xml: xml/index.xml

Note that for docset, a "Makefile" in the html directory will exist, which
can be used to install the docset. The same goes for latex. The 'xml' directory
will contain an 'indexpage.xml' file with the main page contents of the
documentation. The 'man' entries point to the directory containing the
documentation (there is no starting page). The number in the 'man3'
subdirectory is determined by the MAN_EXTENSION setting.

Additionally this contains a list "returncode" containing Doxygen's return code
for each run.
"""

def run(doxyfile, logfile, errorfile, mkdirs=True):
    """
    Run Doxygen over one configuration. The Doxyfile is a Doxyfile object, as
    in the doxyfile module in this package. The log and errorfiles are the full
    paths of where the log and error output should be written to.

    This function returns a dictionary with all the output filenames (such as the
    module's "outputs"), with an extra item "returncode" indicating Doxygen's
    return code.

    @param doxyfile: Run doxygen using the given Doxyfile
    @type doxyfile: Doxyfile
    @param logfile: Name of file to write Doxygen stdout output to
    @type logfile: str
    @param errorfile: Name of file to write Doxygen stderr output to
    @type errorfile: str
    @param mkdirs: Boolean indicating if output and log directories should be
                   created (including parents) or not
    @return: Dictionary with all outputs of the current run and the returncode.
    @rtype: dict
    """
    outdir = path.expandvars(doxyfile.string_setting("OUTPUT_DIRECTORY", '.'))
    if mkdirs:
        # Create directories
        dirs = [
            path.dirname(path.abspath(logfile)),
            path.dirname(path.abspath(errorfile)),
            path.abspath(outdir),
        ]
        for d in dirs:
            if not path.isdir(d):
                makedirs(d)

    # Open log/error files for writing
    flog = open(logfile, 'w')
    ferr = open(errorfile, 'w')
    # Invoke doxygen
    process = subprocess.Popen([doxygen, "-"], stdin=subprocess.PIPE, stdout=flog, stderr=ferr)
    process.communicate(path.expandvars(doxyfile.contents.encode(doxyfile.encoding)))

    flog.close()
    ferr.close()

    current_outputs = dict()
    current_outputs["returncode"] = process.returncode

    if doxyfile.bool_setting("GENERATE_HTML", True): # On by default
        htmldir = path.abspath(path.join(outdir, doxyfile.string_setting("HTML_OUTPUT", "html")))
        htmlext = doxyfile.string_setting("HTML_FILE_EXTENSION", ".html")
        htmlfile = path.join(htmldir, 'index' + htmlext)
        if path.isfile(htmlfile):
            current_outputs["html"] = htmlfile
            # Check if html contains other items, depending on Doxyfile settings
            docset = path.join(htmldir, "Info.plist")
            chm = path.join(htmldir, doxyfile.string_setting("CHM_FILE", "index.chm"))
            chi = path.join(htmldir, "index.chi")
            qhp = path.join(htmldir, "index.qhp")
            qch = path.join(htmldir, doxyfile.string_setting("QCH_FILE", "index.qch"))
            eclipse = path.join(htmldir, "plugin.xml")
            if doxyfile.bool_setting("GENERATE_DOCSET", False) and path.isfile(docset):
                current_outputs['docset'] = docset
            if doxyfile.bool_setting("GENERATE_HTMLHELP", False):
                if path.isfile(chm):
                    current_outputs['chm'] = chm
                if doxyfile.bool_setting("GENERATE_CHI", False) and path.isfile(chi):
                    current_outputs['chi'] = chi
            if doxyfile.bool_setting("GENERATE_QHP", False):
                if path.isfile(qhp):
                    current_outputs['qhp'] = qhp
                if path.isfile(qch):
                    current_outputs['qch'] = qch
            if doxyfile.bool_setting("GENERATE_ECLIPSEHELP", False) and path.isfile(eclipse):
                current_outputs['eclipse'] = eclipse

    if doxyfile.bool_setting("GENERATE_LATEX", True): # On by default
        latexdir = path.abspath(path.join(outdir, doxyfile.string_setting("LATEX_OUTPUT", "latex")))
        latex = path.join(latexdir, 'refman.tex')
        if path.isfile(latex):
            current_outputs['latex'] = latex

    if doxyfile.bool_setting("GENERATE_RTF", False):
        rtfdir = path.abspath(path.join(outdir, doxyfile.string_setting("RTF_OUTPUT", "rtf")))
        rtf = path.join(rtfdir, 'refman.rtf')
        if path.isfile(rtf):
            current_outputs['rtf'] = rtf

    if doxyfile.bool_setting("GENERATE_MAN", False):
        mandir = path.abspath(path.join(outdir, doxyfile.string_setting("MAN_OUTPUT", "man")))
        mansubdir = 'man' + doxyfile.string_setting("MAN_EXTENSION", ".3")[1:]
        man = path.join(mandir, mansubdir)
        if path.isdir(man):
            current_outputs['man'] = man

    if doxyfile.bool_setting("GENERATE_XML", False):
        xmldir = path.abspath(path.join(outdir, doxyfile.string_setting("XML_OUTPUT", "xml")))
        xml = path.join(xmldir, 'index.xml')
        if path.isfile(xml):
            current_outputs['xml'] = xml

    # Now combine outputs with module
    for (name, value) in list(current_outputs.items()):
        outputs[name].append(value)
    # And return data
    return current_outputs

def default_doxyfile():
    """
    Create a new default Doxyfile, using "doxygen -g". This will construct a
    Doxyfile object with the contents of this template.
    @return: A new doxyfile
    @rtype: Doxyfile
    """
    process = subprocess.Popen([doxygen, "-g", "-"],stdout=PIPE)
    data = process.communicate()[0]
    return doxyfile.load_from_string(data)
