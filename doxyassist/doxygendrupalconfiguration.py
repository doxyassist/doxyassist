
#  DoxyAssist - making modular Doxygen documentation.
#  Copyright (C) 2010-2012 Wouter Haffmans (cheetah@simply-life.net)
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
@file
This module provides a Drupal-based Doxygen project type. This generates modular
documentation for Drupal installations.
"""
from .doxygenconfiguration import XMLNS
from .doxygenconfiguration import DoxygenConfiguration
from .doxygenconfiguration import DoxygenProject
import os
from os import path
import re
from . import doxygenrunner
from xml import dom
from xml.dom import minidom
from xml.dom import Node
import copy
from collections import deque

class DoxygenDrupalConfiguration(DoxygenConfiguration):
    """
    Container for a Drupal-specific configuration. This will separate Drupal
    core documentation from contributed modules (see note).

    Modules are automatically discovered and grouped, saving the effort of having
    to specify each module manually.

    Note that modules are expected to be installed in a sites/*/modules directory.

    @note Only modules with a module.info name the same as their parent directory
          will be considered. E.g. to find module "views", the file "views/views.info"
          must exist somewhere within the modules directory. Subdirectories are
          allowed (e.g. modules/contrib/views/views.info is allowed).

          Once a module is found, its subdirectories will not be considered any
          further,
    """
    def __init__(self):
        DoxygenConfiguration.__init__(self)

    def _load_xml_0100(self, dom):
        """
        Load a version 1.0 XML definition as project
        @param dom The XML DOM (DocumentElement) to load
        """
        da = dom.documentElement
        for c in da.childNodes:
            if (c.nodeType != Node.ELEMENT_NODE or c.namespaceURI != XMLNS):
                continue
            if c.localName == "doxygen":
                self._load_doxygen_0100(c)
            elif c.localName == "qtHelp":
                self._load_qthelp_0100(c)
            elif c.localName == "drupal":
                p = DrupalDoxygenProject()
                p.load_xml(c, "1.0", self)
                self.projects.append(p)

class DrupalDoxygenProject(DoxygenProject):
    """
    A Drupal project. This will automatically load subprojects (depending on the
    configuration), for contributed modules and/or themes as well as Drupal core.
    """
    def __init__(self):
        DoxygenProject.__init__(self)
        self.modules = DrupalModuleGroup()
        """ The list of located modules """
        self.themes = DrupalModuleGroup()
        """ The list of located themes """
        self.grouping = list()
        """ A list of data keys to group by. """
        self.build_core = True
        """ Whether to build Core documentation """
        self.build_modules = True
        """ Whether to build contrib module documentation """
        self.build_themes = False
        """ Whether to build contrib theme documentation """
        self.latest_versions_only = True
        """
        If true, only the latest version of a module will be built. If false,
        all found versions of a module are kept, although precise duplicates
        (i.e. the same version of a module in two locations) are discarded.
        """

    def _locate_contrib(self, subdir):
        """ Scan the "sites/*/subdir" directories to discover installed modules/themes. """
        result = []
        processed = []

        for inputdir in self.input:
            sitesdir = path.abspath(path.join(inputdir, "sites"))
            moduledirs = set([ path.realpath(path.abspath(path.join(sitesdir, x, subdir)))
                        for x in os.listdir(sitesdir)
                        if (os.path.isdir(path.join(sitesdir, x))
                            and os.path.isdir(path.join(sitesdir, x, subdir))) ])

            for m in moduledirs:
                for root, dirs, files in os.walk(m, topdown=True, followlinks=True):
                    # Prevent loops or double catches because of symlinks
                    absroot = path.realpath(path.abspath(root))
                    if absroot in processed:
                        del dirs[:]
                        continue
                    processed.append(absroot)

                    # Find first .info file
                    infos = [ f for f in files if f[-5:] == ".info" ]
                    if len(infos):
                        del dirs[:] # Stop recursion
                        module = DrupalModule(path.join(root, infos[0]))
                        # Check that we haven't already loaded precisely the same
                        # module (including version) from an earlier location
                        # Note that we do still want to keep different versions of
                        # the same module for now
                        if len([m for m in result if
                                (m.info["core"] == module.info["core"] and
                                 m.info["basename"] == module.info["basename"] and
                                 m.info["version"] == module.info["version"])]) == 0:
                            result.append(module)

        return result

    def locate_modules(self):
        """ Scan the "sites/*/modules" directories to discover installed modules. """
        self.modules.extend(self._locate_contrib("modules"))

    def locate_themes(self):
        """ Scan the "sites/*/themes" directories to discover installed themes. """
        self.themes.extend(self._locate_contrib("themes"))

    def prepare(self):
        """
        Override to load and create subprojects of modules and themes as
        necessary.
        """

        if self.build_core:
            # Add "Core" pseudo-subproject
            # TODO: Make "Core" name configurable?
            core = DoxygenProject("Core", '', self, self.config)
            if not self.version_specific:
                core.version = self.version
            core.version_specific = (not self.version_specific) or (not self.latest_versions_only);
            core.input = copy.copy(self.input)
            core.exclude = copy.copy(self.exclude)
            # Exclude all sites/* directories
            core.exclude.extend([ path.join(i, "sites") for i in self.input ])
            core.namespace = ''
            self.projects.append(core)

        if self.build_modules:
            self.locate_modules()
            self.load_subprojects(self.modules, "contrib.modules")
        if self.build_themes:
            self.locate_themes()
            self.load_subprojects(self.themes, "contrib.themes")

        self.skip = True

    def load_subprojects(self, contrib_list, namespace):
        """
        Load subprojects from a group of contributed modules or themes.
        This will apply subgrouping and bundle the modules into a
        """
        if self.latest_versions_only:
            contrib_list.filter_dupes()
        groups = [contrib_list]
        grouping = deque(self.grouping)
        while len(grouping) > 0:
            groupby = grouping.popleft()
            groups = [ sg for g in groups for sg in list(g.split(groupby).values()) ]

        for g in [ g for g in groups if g.value == None ]:
            # Use basename as default value for single-module groups, otherwise use
            # Miscellaneous as project name
            # TODO Make "Miscellaneous" name configurable?
            g.value = g.modules[0].info["basename"] if len(g.modules) == 1 else 'Miscellaneous'

        groups.sort(key=(lambda g : g.value))

        for g in groups:
            # Version to be filled in later, for now just do name, parent and config
            p = DoxygenProject(g.value, '', self, self.config)
            # Only version specific builds if group contains exactly one module
            if len(g.modules) == 1:
                p.version = g.modules[0].info["version"]
                # Make version specific if we may build multiple versions of the
                # same module
                p.version_specific = (not self.latest_versions_only)
            else:
                p.version = ""
                p.version_specific = False

            p.input = [ m.info["directory"] for m in g.modules ]
            p.exclude = copy.copy(self.exclude)

            p.namespace = namespace
            self.projects.append(p)

    def load_xml(self, elt, version="1.0", config = None):
        """
        Load the drupal tag from an XML definition. This inherits all the base
        configuration, but additionally has settings for:
        - What info items to group modules by (any key that appears in .info files,
          "basename" and "directory")
        - Whether the build core/contributed modules/themes documentation.
        - Whether to keep two different versions of the same module/theme in
          the documentation, or to keep only the latest one.
        - The sub-namespace to use for contributed modules/themes

        @param elt The container element with project nodes
        @param version The XML file's DoxyAssist version.
        @param config The main configuration to load.

        @note A Drupal project cannot have child or parent projects. Subprojects
              in the XML file will not be loaded.
        @note If a "VersionSpecific" build is reque
        """
        DoxygenProject.load_xml(self, elt, version, None, config, False)

        # Default name
        if (self.name == "" or self.name == None):
            self.name = "Drupal"

        # Drupal specific settings
        for c in elt.childNodes:
            if (c.nodeType != Node.ELEMENT_NODE or c.namespaceURI != XMLNS):
                continue
            elif c.localName == "groupBy":
                self.grouping.append(DoxygenProject._element_text(c))
            elif c.localName == "buildCore":
                self.build_core = DoxygenProject._element_bool(c)
            elif c.localName == "buildModules":
                self.build_modules = DoxygenProject._element_bool(c)
            elif c.localName == "buildThemes":
                self.build_themes = DoxygenProject._element_bool(c)
            elif c.localName == "latestContribOnly":
                self.latest_versions_only = DoxygenProject._element_bool(c)

        if len(self.grouping) == 0:
            self.grouping.append("core")
            self.grouping.append("name")

class DrupalModuleGroup(object):
    """ A group of modules, which can be split up into subgroups etc. """
    def __init__(self, key = None, value = None):
        self.key = key
        """
        The grouping key (property name) that is used for this group
        """
        self.value = value
        """
        The grouping value for all modules in this group. I.e. for all modules
        m it holds that m.info[key] == value
        """
        self._modules = list()

    def _get_modules(self):
        return self._modules
    modules = property(_get_modules)
    """
    The list of modules (read-only property). Use the functions in this class
    to manipulate the list.
    """

    def add_module(self, module):
        """
        Add a new module to the group. This verifies that the module is
        valid for this group (i.e.:
        - key == None
        - value == None and (module.info[key] == None or key not in module.info)
        - module.info[key] == value
        )
        """
        self.extend([module])

    def extend(self, modulelist):
        """
        Add a set of modules to the group, verifying the modules in the process
        like add_module()
        """
        if (self.key == None):
            self._modules.extend(modulelist)
        elif (self.value == None):
            self._modules.extend([ m for m in modulelist if
                                  (self.key not in m.info or m.info[self.key] == None or m.info[self.key] == '')
                                 ])
        else:
            self._modules.extend([ m for m in modulelist if
                                  (self.key in m.info and m.info[self.key] == self.value)
                                 ])

    def split(self, key):
        """
        Split the group into subgroups. For each unique value of module.info[key]
        in the modules list, a subgroup will be created (with the appropriate
        key and value). The matching modules will be placed in this list.

        A special group with key "__none__" in the dictionary is added, containing
        all modules which do not have a value set for the given key. Note that this
        may also include modules whose value for key is also "__none__", but
        that is considered very unlikely.
        """
        groups = dict()
        for value in set([ m.info[key] for m in self._modules if key in m.info ]):
            groups[value] = DrupalModuleGroup(key, value)
            groups[value].extend([ m for m in self._modules if key in m.info and m.info[key] == value ])

        none_group = [m for m in self._modules if key not in m.info or m.info[key] == None]
        if len(none_group) > 0:
            groups["__none__"] = DrupalModuleGroup(key, None)
            groups["__none__"].extend(none_group)
        return groups

    def filter_dupes(self):
        """
        Filter duplicate modules. If two modules with the same core version and
        name are found, only the latest version is kept.
        """
        if (len(self._modules) == 0):
            return

        # Sort on version (tertiary key) - newest at the end, so we can simply take the first
        # we come across when scanning the list in reverse later
        self._modules.sort(key=(lambda m : m.info["version"]))
        # Then name (secondary)
        self._modules.sort(key=(lambda m : m.info["basename"]))
        # And core (primary)
        self._modules.sort(key=(lambda m : m.info["core"]))
        last = self._modules[-1]
        for i in range(len(self._modules)-2, -1, -1): # Scan from end as indexes may change
            if last == self._modules[i]: # compares name and core
                del self._modules[i]
            else:
                last = self._modules[i]

class DrupalModule(object):
    info_re = re.compile(
                '^\\s*                           # Start at the beginning of a line, ignoring leading whitespace\n\
                ((?:\n\
                    [^=;\\[\\]]|                    # Key names cannot contain equal signs, semi-colons or square brackets,\n\
                    \\[[^\\[\\]]*\\]                  # unless they are balanced and not nested\n\
                )+?)\n\
                \\s*=\\s*                         # Key/value pairs are separated by equal signs (ignoring white-space)\n\
                (?:\n\
                    ("(?:[^"]|(?<=\\\\)")*")|     # Double-quoted string, which may contain slash-escaped quotes/slashes\n\
                    (\'(?:[^\']|(?<=\\\\)\')*\')| # Single-quoted string, which may contain slash-escaped quotes/slashes\n\
                    ([^\\r\\n]*?)                   # Non-quoted string\n\
                )\s*$                           # Stop at the next end of a line, ignoring trailing whitespace',
                re.M | re.S | re.X)
    arraykey_re = re.compile(r'\]?\[')

    """ A definition of a Drupal module. """
    def __init__(self, file = None):
        """ Initialize the module. If the file is given, the """
        self.info = dict()

        if (file != None):
            self.load_info(file)

    def __eq__(self, other):
        core_result = (self.info["core"] == other.info["core"])
        return core_result if not core_result else (self.info["basename"] == other.info["basename"])

    def load_info(self, filename):
        """
        Load a Drupal module's information from an .info file. Usually this will
        at least load information like the module's name, core version (6.x and later),
        version, description, and more. Additionally the directory, infofile,
        and basename (without .info extension) will be loaded. Everything can be
        accessed through the "info" dictionary.

        Largely based on Dupal's (6.x) drupal_parse_info_file() (includes/common.inc)
        """
        with open(filename, mode='r') as f:
            self.info = dict()
            self.info["basename"] = path.splitext(path.basename(filename))[0]
            self.info["directory"] = path.dirname(filename)

            contents = f.read()
            for m in DrupalModule.info_re.finditer(contents):
                key = m.group(1) if m.group(1) != None else ''
                value1 = m.group(2) if m.group(2) != None else ''
                value2 = m.group(3) if m.group(3) != None else ''
                value3 = m.group(4) if m.group(4) != None else ''
                value = value1[1:-1] + value2[1:-1] + value3

                # Parse array syntax
                keys = DrupalModule.arraykey_re.split(key.rstrip(']'))
                last = keys.pop()
                parent = self.info

                # Create nested arrays.
                for key in keys:
                    if key == '':
                        key = len(parent)
                    if (key not in parent or not isinstance(parent[key], (dict))):
                        parent[key] = dict()
                    parent = parent[key]

                # Handle PHP constants: not in Python :)
                # Insert actual value
                if last == '':
                    last = len(parent)
                parent[last] = value

            # Assume '5.x' core if none was given (as of Drupal 6 the "core"
            # key is required in the .info files); first try to deduce core from
            # version though
            if "core" not in self.info:
                if "version" in self.info:
                    self.info["core"] = self.info["version"][:3]
                else:
                    self.info["core"] = "5.x"
            if "version" not in self.info:
                # Assume -dev version, if no version was in info file
                self.info["version"] = self.info["core"] + "-dev"

