
#  DoxyAssist - making modular Doxygen documentation.
#  Copyright (C) 2010-2012 Wouter Haffmans (cheetah@simply-life.net)
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
@file
The qhelp module provides settings and information about a QHelpCollectionProject.

The QHelpCollection class has settings, including lists of Qt Compressed Help
files, to generate a QHelpCollection.

Additionally there are methods to execute the "qcollectiongenerator" command
using a QHelpCollection.

@todo Support for reading existing qhcp (and qhc?) files
"""

from os import makedirs
from os import symlink
from os import path
from xml import dom
from xml.dom import minidom
from xml.dom import Node
from xml.dom.minidom import getDOMImplementation
import shutil
import subprocess
import os

try:
    from PyQt5.QtCore import QStandardPaths
    _havePyQt4 = False
    _havePyQt5 = True
except ImportError:
    _havePyQt5 = False
    try:
        from PyQt4.QtGui import QDesktopServices
        _havePyQt4 = True
    except ImportError:
        _havePyQt4 = False

qcollectiongenerator = "/usr/bin/qcollectiongenerator"
""" The command to use to execute "qcollectiogenerator" """

class QHelpCollection(object):
    """
    A QHelpCollection gives a definition of a Qt Help Collection which can be
    viewed using the Qt Assistant.

    The class has an output filename (.qhc file), and a project file (usually the
    same name, but with a .qhcp extension). Various settings provide information
    about the assistant's UI.

    The class maintains two lists of files. The first is one with new files that
    are yet to be copied to a path relative from the .qhc file. Existing ones are
    those that were copied to the proper location already.

    When copying the files, the "qch_path_prefix" member will be used to determine
    in which subdirectory to copy the Qt collection files to. If "copy_files" is
    set to False, instead of copying the files, symbolic links will be created.

    To add a .qch file to the collection, put it in the new_files list (or use
    the add_qch() function). Use prepare_files() to copy or link the new qch files
    and build() to execute the qcollectiongenerator.
    """
    def __init__(self, filename):
        self._project_file = path.splitext(filename)[0] + '.qhcp'
        self._custom_project_file = False
        self.filename = filename # _custom_project_file must be initialized first (see set_filename())
        self.qch_path_prefix = ''
        self.assistant = {
            'title': '',
            'homePage': '',
            'startPage': '',
            'currentFilter': '',
            'applicationIcon': '',
            'enableFilterFunctionality': True,
            'enableFilterFunctionalityVisible': True,
            'enableDocumentationManager': True,
            'enableAddressBar': True,
            'enableAddressBarVisible': False,
            'aboutMenuTextDefault': '',
            'aboutMenuTextLanguages': dict(),
            'aboutDialogFileDefault': '',
            'aboutDialogFileLanguages': dict(),
            'aboutDialogIcon': '',
            'cacheDirectory': '',
        }
        self._new_files = []
        self._existing_files = []
        self._existing_qhp_files = []
        self._new_qhp_files = []
        self.copy_action = "copy"

    def get_filename(self):
        return self._filename

    def set_filename(self, value):
        self._filename = value
        if (not self._custom_project_file):
            self._project_file = path.splitext(value)[0] + '.qhcp'

    def get_project_file(self):
        return self._project_file

    def set_project_file(self, value):
        if (value != self._project_file):
            self._custom_project_file = True
            self._project_file = value

    filename = property(get_filename, set_filename)
    """ The filename of the help collection to create """
    project_file = property(get_project_file, set_project_file)
    """ The filename of the help project to create. Based on the filename by default. """

    def _element_text(self, elt):
        """ Helper: Get the text contents of an XML element """
        elt.normalize()
        data = ''
        for c in elt.childNodes:
            if c.nodeType == Node.TEXT_NODE:
                data += c.nodeValue
        return data

    def load_assistant_settings(self, element, ns = None, version="1.0"):
        """
        Load assistant settings from an XML element.

        The XML structure within the element should be as outlined at
        http://doc.trolltech.com/4.6/assistant-custom-help-viewer.html#creating-a-custom-help-collection-file
        Optionally a namespace the elements appear in can be provided.
        """
        if (version > "1.0"):
            raise ValueError("Unsupported version. Cannot load Qt Assistant settings")

        for c in element.childNodes:
            if (c.nodeType != Node.ELEMENT_NODE or (ns != None and c.namespaceURI != ns)):
                continue

            if (c.localName in ["title", "homePage", "startPage",
                                "currentFilter", "applicationIcon", "cacheDirectory"]):
                # String settings
                self.assistant[c.localName] = self._element_text(c)

            if (c.localName in ["enableFilterFunctionality", "enableDocumentationManager",
                                "enableAddressBar"]):
                strValue = self._element_text(c)
                if not (strValue in ['true', 'false', '1', '0']):
                    raise ValueError('Value %s cannot be converted to a boolean' % strValue)
                self.assistant[c.localName] = strValue in ['true', '1']

                if (c.localName in ["enableFilterFunctionality", "enableAddressBar"] and c.hasAttribute("visible")):
                    strAttr = c.getAttribute("visible");
                    if not (strAttr in ['true', 'false', '1', '0']):
                        raise ValueError('Value %s for %s:visible cannot be converted to a boolean' % (strAttr, c.localName))
                    self.assistant[c.localName + 'Visible'] = strAttr in ['true', '1']
                elif (c.localName in ["enableFilterFunctionality", "enableAddressBar"]):
                    self.assistant[c.localName + 'Visible'] = self.assistant[c.localName]

            if (c.localName == "aboutMenuText"):
                # Traverse child elements of aboutMenuText
                self._load_aboutMenuText(c, ns)

            if (c.localName == "aboutDialog"):
                # Traverse child elements of aboutMenuText
                self._load_aboutDialog(c, ns)

    def _load_aboutMenuText(self, element, ns):
        """ Load the aboutMenuText assistant setting from an XML element """
        for c in element.childNodes:
            if (c.nodeType != Node.ELEMENT_NODE or (ns != None and c.namespaceURI != ns)):
                continue
            if (c.localName == "text" and (not c.hasAttribute("language"))):
                value = self._element_text(c)
                self.assistant["aboutMenuTextDefault"] = value
            elif  (c.localName == "text" and c.hasAttribute("language")):
                language = c.getAttribute("language")
                value = self._element_text(c)
                self.assistant["aboutMenuTextLanguages"][language] = value

    def _load_aboutDialog(self, element, ns):
        """ Load the aboutDialog assistant settings from an XML element """
        for c in element.childNodes:
            if (c.nodeType != Node.ELEMENT_NODE or (ns != None and c.namespaceURI != ns)):
                continue
            if (c.localName == "file" and (not c.hasAttribute("language"))):
                value = self._element_text(c)
                self.assistant["aboutDialogFileDefault"] = value
            elif  (c.localName == "file" and c.hasAttribute("language")):
                language = c.getAttribute("language")
                value = self._element_text(c)
                self.assistant["aboutDialogFileLanguages"][language] = value
            elif (c.localName == "icon"):
                value = self._element_text(c)
                self.assistant["aboutDialogIcon"] = value

    def add_file(self, filename):
        """
        Safely add a new .qch file to the collection.

        This first checks that the file is not already added (i.e. no file with
        the same basename is in the new_files list). If the file's target file
        is already in the existing_files list, the old file will be replaced.

        Additionally it is checked that the target location
        of the file is in a subdirectory or in the same directory as where the
        .qhc file will be located. This will break if the qch_path_prefix is set
        incorrectly (e.g. to '../'), and will throw an exception.

        If it is a new file, it is added.

        @return True if the file was added, False if it was not
        """
        homedir = path.abspath(path.dirname(path.expandvars(self.filename)))
        target = path.join(homedir, path.expandvars(self.qch_path_prefix), path.basename(filename))
        target = path.abspath(target)
        # Make sure target is in valid subdirectory
        prefix = path.commonprefix([homedir, path.dirname(target)])
        if prefix != homedir:
            raise Exception("QHelpCollection qch_path_prefix is incorrect: files will "
                            "be stored in parent directory of help collection file.")
        if path.realpath(filename) in [ path.realpath(f) for f in self._new_files ]:
            return False # File already in list
        if target[len(prefix):] in self._existing_files:
            self._existing_files.remove(target[len(prefix):])
        self._new_files.append(filename)
        return True

    def add_project(self, filename):
        """
        Add a Qt Help Project (which generates a Qt Compressed Help file) to the
        project.
        """
        homedir = path.abspath(path.dirname(path.expandvars(self.filename)))
        target = path.join(homedir, path.expandvars(self.qch_path_prefix), path.basename(filename))
        target = path.abspath()
        # Make sure target is in valid subdirectory
        prefix = path.commonprefix([homedir, path.dirname(target)])
        if prefix != homedir:
            raise Exception("QHelpCollection qch_path_prefix is incorrect: files will "
                            "be stored in parent directory of help collection file.")
        if path.realpath(filename) in [ path.realpath(f) for f in self._new_files ]:
            return False # File already in list
        if target[len(prefix):] in self._existing_files:
            self._existing_files.remove(target[len(prefix):])
        self._new_qhp_files.append(filename)


    def copyfiles(self):
        """
        Copy new files to their destinations

        This will copy, symlink or move the new qch files to
        the correct destination. Note that the qhc filename must be set, as the
        target directory is based on that.
        """

        # Make sure destination exists
        targetdir = path.join(path.dirname(path.expandvars(self.filename)), path.expandvars(self.qch_path_prefix))
        if (not path.isdir(targetdir)):
            makedirs(targetdir)

        # Copy new files
        for f in self._new_files:
            self._copyfile(f)
            self._existing_files.append(path.join(path.expandvars(self.qch_path_prefix), path.basename(f)))
        # Clear list
        self._new_files = []

        # Copy QHP files
        for f in self._new_qhp_files:
            self._copyfile(f)
            self._existing_qhp_files.append(path.join(path.expandvars(self.qch_path_prefix), path.basename(f)))
        # Clear list
        self._new_qhp_files = []

    def clearcache(self):
        print('Clearing Help Collection cache...')
        if (not _havePyQt4 and not _havePyQt5):
            print('PyQt4 and PyQt5 not found; unable to clear user cache for Qt Help Collection file.')
            return

        if (_havePyQt4):
            maindir = path.normpath(str(QDesktopServices.storageLocation(QDesktopServices.DataLocation)))
            subdir = self.assistant['cacheDirectory'] if len(self.assistant['cacheDirectory']) > 0 else 'Trolltech/Assistant'
            cachedir = path.join(maindir, subdir)
        elif (_havePyQt5):
            maindir = path.normpath(str(QStandardPaths.writableLocation(QStandardPaths.DataLocation)))
            subdir = self.assistant['cacheDirectory'] if len(self.assistant['cacheDirectory']) > 0 else 'QtProject/Assistant'
            cachedir = path.join(maindir, subdir)

        # Make sure cacheDir is within the mainDir
        if (path.commonprefix([maindir, cachedir]) != maindir):
            print('Cannot clear cache outside user\'s data directory ({0})'.format(cachedir))
            return

        if (path.isdir(cachedir)):
            (projname, extension) = path.splitext(path.basename(path.expandvars(self.filename)))
            projcachedir = path.join(cachedir, "." + projname)
            projcachefile = path.join(cachedir, projname + extension)
            if (path.isdir(projcachedir)):
                shutil.rmtree(path.join(cachedir, "." + projname))
            if (path.exists(projcachefile)):
                os.remove(path.join(cachedir, projname + extension))
        print('User cache for Qt Help Collection cleared ({0}).'.format(cachedir))

    def _copyfile(self, filename):
        """
        Copy a file to the proper location
        This will symlink, move or copy (default) the file to the directory of
        the self.filename, in a subdirectory determined by qch_path_prefix.
        """
        homedir = path.dirname(path.expandvars(self.filename))
        target = path.join(homedir, path.expandvars(self.qch_path_prefix), path.basename(filename))
        target = path.abspath(target)
        if self.copy_action == "symlink":
            symlink(filename, target)
        elif self.copy_action == "move":
            shutil.move(filename, target)
        else:
            # Copy actual file, not the symlink etc, hence realpath()
            shutil.copy(path.realpath(filename), target)

    def _add_optional_tag(self, name, value, doc, parent, attrs = None):
        """
        Small helper to add an assistant XML tag, if a value is set
        @param name Name of the tag to add
        @param value The string value to put in the tag
        @param doc The XML DOM document to add the tag to
        @param parent The XML parent element to add the tag to
        @param attrs A dictionary of attributes to add
        @return The created DomElement if one was added, None otherwise
        """
        if (attrs == None):
            attrs = dict()
        if (value == None or str(value) == ''):
            return None
        elt = doc.createElement(name)
        elt.appendChild(doc.createTextNode(str(value)))
        for (attrName, attrValue) in list(attrs.items()):
            elt.setAttribute(str(attrName), str(attrValue))
        parent.appendChild(elt)
        return elt

    def _bool_to_xmlstring(self, value):
        """ Turn a boolean into a "true" or "false" string """
        return "true" if value else "false"

    def projectdoc(self):
        """
        Create the project's XML dom. This can be saved to a project file.
        Note: newly added files must first be processed with copyfiles() if they
        need to be taken into account
        @return A DOM document created using the xml.dom.minidom implementation
        """
        # Create XML
        impl = getDOMImplementation()
        doc = impl.createDocument(None, "QHelpCollectionProject", None)
        qhcp = doc.documentElement
        qhcp.setAttribute("version", "1.0")

        # Assistant settings
        assistant = doc.createElement("assistant")

        self._add_optional_tag("title", self.assistant['title'], doc, assistant)
        self._add_optional_tag("homePage", self.assistant['homePage'], doc, assistant)
        self._add_optional_tag("startPage", self.assistant['startPage'], doc, assistant)
        self._add_optional_tag("applicationIcon", self.assistant['applicationIcon'], doc, assistant)
        self._add_optional_tag("currentFilter", self.assistant['currentFilter'], doc, assistant)
        attrs = {'visible': self._bool_to_xmlstring(
                                    self.assistant['enableFilterFunctionalityVisible'])}
        self._add_optional_tag("enableFilterFunctionality",
                               self._bool_to_xmlstring(self.assistant['enableFilterFunctionality']),
                               doc, assistant, attrs
                               )
        self._add_optional_tag("enableDocumentationManager",
                               self._bool_to_xmlstring(self.assistant['enableDocumentationManager']),
                               doc, assistant)
        attrs = {'visible': self._bool_to_xmlstring(self.assistant['enableAddressBarVisible'])}
        self._add_optional_tag("enableAddressBar",
                               self._bool_to_xmlstring(self.assistant['enableAddressBar']),
                               doc, assistant, attrs)
        if (self.assistant['aboutMenuTextDefault'] != '' or len(self.assistant['aboutMenuTextLanguages']) > 0):
            aboutMenuText = doc.createElement("aboutMenuText")
            self._add_optional_tag("text", self.assistant['aboutMenuTextDefault'], doc, aboutMenuText)
            for (lang, text) in list(self.assistant['aboutMenuTextLanguages'].items()):
                attrs = {'language': str(lang)}
                self._add_optional_tag("text", str(text), doc, aboutMenuText, attrs)
            assistant.appendChild(aboutMenuText)

        if (self.assistant['aboutDialogFileDefault'] != '' or len(self.assistant['aboutDialogFileLanguages']) > 0):
            aboutDialog = doc.createElement("aboutDialog")
            self._add_optional_tag("file", self.assistant['aboutDialogFileDefault'], doc, aboutDialog)
            for (lang, filename) in list(self.assistant['aboutDialogFileLanguages'].items()):
                attrs = {'language': str(lang)}
                self._add_optional_tag("file", str(filename), doc, aboutDialog, attrs)
            self._add_optional_tag("icon", self.assistant['aboutDialogIcon'], doc, aboutDialog)
            assistant.appendChild(aboutDialog)

        self._add_optional_tag("cacheDirectory", self.assistant['cacheDirectory'], doc, assistant)
        qhcp.appendChild(assistant)

        # Add Files
        docFiles = doc.createElement("docFiles")
        if len(self._existing_qhp_files) > 0:
            generate = doc.createElement("generate")
            for qhp_file in self._existing_qhp_files:
                # Create <file><input>*.qhp</input><output>*.qch</output></file>
                (base, ext) = path.splitext(qhp_file)
                qch_file = base + '.qch'
                fileelt = doc.createElement("file")

                inputelt = doc.createElement("input")
                inputelt.appendChild(doc.createTextNode(qhp_file))
                fileelt.appendChild(inputelt)

                outputelt = doc.createElement("output")
                outputelt.appendChild(doc.createTextNode(qch_file))
                fileelt.appendChild(outputelt)

                generate.appendChild(fileelt)
            docFiles.appendChild(generate)

        if len(self._existing_files) > 0:
            register = doc.createElement("register")
            for filename in self._existing_files:
                fileelt = doc.createElement("file")
                fileelt.appendChild(doc.createTextNode(filename))
                register.appendChild(fileelt)
            docFiles.appendChild(register)

        qhcp.appendChild(docFiles)
        doc.appendChild(qhcp)
        return doc

    def generate(self):
        """
        Create the Qt Help Collection
        This creates or overwrites the project file of the collection. Then it
        invokes `qcollectiongenerator` to create the actual collection.
        """
        self.copyfiles()
        doc = self.projectdoc()
        f = open(path.expandvars(self.project_file), 'w')
        # QCollectionGenerator doesn't like spaces around filenames
        doc.writexml(f, "", "", "", "UTF-8")
        f.close()

        # Call qcollectiongenerator
        result = subprocess.call([qcollectiongenerator, "-o", path.expandvars(self.filename), path.expandvars(self.project_file)])

        # Clear user's cache
        self.clearcache();
        return result
