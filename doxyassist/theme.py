
#  DoxyAssist - making modular Doxygen documentation.
#  Copyright (C) 2010-2012 Wouter Haffmans (cheetah@simply-life.net)
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
@file
This module provides information regarding DoxyAssist HTML themes. Themes can be
used to change the HTML output of Doxygen.
"""

import configparser
from os import path

class DoxyAssistTheme(object):
    """
    A DoxyAssistTheme defines the HTML files to be used as header, footer,
    stylesheet and additional files. This allows templates to be easily reused
    for different projects, without having to manually configure the necessary
    Doxygen settings. Themes can be distributed as a package, and projects
    can simply point to the right path to have all the settings applied
    automatically.

    Note that for the theme's "extrafiles" settings to work, at least
    doxygen 1.7.4 must be used. In general, themes will work better with at least
    doxygen 1.7.4 than any previous version, due to the many improvements regarding
    custom headers and footers since then.
    """

    def __init__(self, directory):
        self._path = path.abspath(directory)
        # Set defaults
        self._files = {
            "header": "",
            "footer": "",
            "stylesheet": "",
        }
        self._extrafiles = list()
        self._colorstyle = {
            "hue": 220,
            "sat": 100,
            "gamma": 80
        }
        self._forced_settings = dict()

        # Load theme file
        if (path.isfile(path.join(self._path, "theme.ini"))):
            self.load_from_ini(path.join(self._path, "theme.ini"))

    def get_header(self):
        return path.join(self._path, self._files["header"])
    def set_header(value):
        self._files["header"] = value
    header = property(get_header, set_header)
    """ The file containing the HTML Header for this theme. This returns the absolute path.
        If the value is set as a relative path, it is relative to the theme's main path. """

    def get_footer(self):
        return path.join(self._path, self._files["footer"])
    def set_footer(value):
        self._files["footer"] = value
    footer = property(get_footer, set_footer)
    """ The file containing the HTML footer for this theme. This returns the absolute path.
        If the value is set as a relative path, it is relative to the theme's main path. """

    def get_stylesheet(self):
        return path.join(self._path, self._files["stylesheet"])
    def set_stylesheet(self, value):
        self._files["stylesheet"] = value
    stylesheet = property(get_stylesheet, set_stylesheet)
    """ The file containing the CSS style sheet for this theme. This returns the absolute path.
        If the value is set as a relative path, it is relative to the theme's main path. """

    def get_layout(self):
        return path.join(self._path, self._files["layout"])
    def set_layout(self, value):
        self._files["layout"] = value
    layout = property(get_layout, set_layout)
    """ The file containing the Doxygen layout description. This returns an absolute path.
        If the value is set as a relative path, it is relative to the theme's main path. """

    def get_extrafiles(self):
        return [ path.join(self._path, f) for f in self._extrafiles ]
    def set_extrafiles(self, value):
        self._extrafiles = value
    extrafiles = property(get_extrafiles, set_extrafiles)
    """ Extra files to copy to the Doxygen HTML output directory. This returns the absolute path
        of the files. Files set with relative paths will be made absolute. """

    def get_hue(self):
        return self._colorstyle["hue"]
    def set_hue(self, value):
        self._colorstyle["hue"] = value
    hue = property(get_hue, set_hue)
    """ The base hue to use for colorization of Doxygen-generated graphics. """

    def get_sat(self):
        return self._colorstyle["sat"]
    def set_sat(self, value):
        self._colorstyle["sat"] = value
    sat = property(get_sat, set_sat)
    """ The base saturation to use for colorization of Doxygen-generated graphics. """

    def get_gamma(self):
        return self._colorstyle["gamma"]
    def set_gamma(self, value):
        self._colorstyle["gamma"] = value
    gamma = property(get_gamma, set_gamma)
    """ The base gamma to use for colorization of Doxygen-generated graphics. """

    def get_forced_settings(self):
        return self._forced_settings
    forced_settings = property(get_forced_settings)

    def load_from_ini(self, file):
        """
        Load theme configuration from ini file
        """
        self._forced_settings = dict()

        config = configparser.RawConfigParser()
        config.read(file)

        for setting in [ "header", "footer", "stylesheet", "layout" ]:
            self._files[setting] = config.get("DoxyAssistTheme", setting) if config.has_option("DoxyAssistTheme", setting) else ""

        i = 1
        self._extrafiles = list()
        while (config.has_option("DoxyAssistExtraFiles", "file" + str(i))):
            self._extrafiles.append(config.get("DoxyAssistExtraFiles", "file" + str(i)))
            i += 1

        colordefaults = { "hue": 220, "sat": 100, "gamma": 80 }
        for setting in [ "hue", "sat", "gamma" ]:
            self._colorstyle[setting] = config.getint("DoxyAssistColorStyle", setting) if config.has_option("DoxyAssistColorStyle", setting) else colordefaults[setting]

        overrides = config.options('DoxyAssistForceSettings') if config.has_section('DoxyAssistForceSettings') else dict()
        for key in overrides:
            self._forced_settings[key.upper()] = config.get('DoxyAssistForceSettings', key)

    def apply2doxyfile(self, doxyfile):
        if (self.header != ''):
            doxyfile.set_setting("HTML_HEADER", self.header)
        if (self.footer != ''):
            doxyfile.set_setting("HTML_FOOTER", self.footer)
        if (self.stylesheet != ''):
            doxyfile.set_setting("HTML_STYLESHEET", self.stylesheet)
        if (self.layout != ''):
            doxyfile.set_setting("LAYOUT_FILE", self.layout)

        if len(self.extrafiles) > 0:
            allfiles = doxyfile.setting("HTML_EXTRA_FILES")
            allfiles.extend(self.extrafiles)
            doxyfile.set_setting("HTML_EXTRA_FILES", allfiles)

        doxyfile.set_setting("HTML_COLORSTYLE_HUE", self.hue);
        doxyfile.set_setting("HTML_COLORSTYLE_SAT", self.sat);
        doxyfile.set_setting("HTML_COLORSTYLE_GAMMA", self.gamma);

        for (key, value) in self.forced_settings.items():
            doxyfile.set_setting(key, value)
