
#  DoxyAssist - making modular Doxygen documentation.
#  Copyright (C) 2010-2012 Wouter Haffmans (cheetah@simply-life.net)
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
@file
This module provides Doxygen project types and methods to load them.

The projects can be used by the DoxygenRunner to load the Doxyfile and
other settings.
"""

from .doxyfile import Doxyfile
from .theme import DoxyAssistTheme
from .qhelp import QHelpCollection
from . import doxygenrunner
from xml import dom
from xml.dom import minidom
from xml.dom import Node
from collections import deque
from os import path
from copy import copy
from sys import stdout
import re

XMLNS = "http://simply-life.net/doxyassist/doxyassist.xsd"
""" The common DoxygenProject XML Namespace - actual file still to be made :) """

_norm_pattern = re.compile(r'[^a-zA-Z0-9\-\_]')
""" The normalization regex patterns """
_norm_dot_pattern = re.compile(r'[^a-zA-Z0-9\-\.]')
""" The normalization regex patterns. This version keeps dots in the names. """

class DoxygenConfiguration(object):
    """
    A definition for a Doxygen configuration

    A DoxygenConfiguration defines one or more Doxygen projects for use with the
    DoxygenRunner. The DoxygenRunner will read the necessary settings,
    including projects and subprojects, and execute Doxygen with the specified
    settings.
    """

    def __init__(self):
        """ Initialize an empty generic Doxygen Project """
        self._name = ''
        self._doxyfile = None
        self._qthelp = None
        self._projects = list()
        self._force_htmltheme = None

    def get_name(self):
        return self._name
    def set_name(self, value):
        self._name = value
    name = property(get_name, set_name)

    def get_doxyfile(self):
        return self._doxyfile
    def set_doxyfile(self, value):
        self._doxyfile = value
    doxyfile = property(get_doxyfile, set_doxyfile)

    def get_qthelp(self):
        return self._qthelp
    def set_qthelp(self, value):
        self._qthelp = value
    qthelp = property(get_qthelp, set_qthelp)

    def get_projects(self):
        return self._projects
    def set_projects(self, value):
        self._projects = value
    projects = property(get_projects, set_projects)

    def get_force_htmltheme(self):
        return self._force_htmltheme
    def set_force_htmltheme(self, value):
        self._force_htmltheme = value
    force_htmltheme = property(get_force_htmltheme, set_force_htmltheme)

    def doxygen(self, debug):
        """
        Run Doxygen over all the projects in this configuration. The combined
        outputs will be returned as list
        """
        projects = deque()
        projects.extend(self.projects)
        outputs = list()

        # Init theme override
        override_theme = DoxyAssistTheme(self._force_htmltheme) if (self._force_htmltheme != None) else None

        while len(projects) > 0:
            p = projects.popleft()
            p.prepare()

            if p.skip:
                projects.extend(p.projects)
                continue

            print("Running doxygen for project {0}...".format(p.full_name + " " + p.version), end=' ')
            stdout.flush()

            p.applydoxysettings()
            # Override theme
            if (override_theme != None):
                override_theme.apply2doxyfile(p.doxyfile)

            if (debug):
              doxyfilename = path.join(path.abspath(path.expandvars(p.logdir)), 'Doxyfile-' + p.full_namespace)
              with open(doxyfilename, 'w') as f:
                f.write(p.doxyfile.contents)

            output = doxygenrunner.run(p.doxyfile, p.logfile, p.errorlogfile)

            if output["returncode"] == 0:
                print("OK!")
            else:
                print("ERROR!")
                print("Please see the log file {0} and error log {1} for details why doxygen may have failed (error code: {c})".format(
                      p.logfile, p.errorlogfile, c=output["returncode"]))

            projects.extend(p.projects)
            outputs.append(output)

        return outputs

    @classmethod
    def load_xml(cls, dom):
        """
        Load a definition from an XML DOM. Do not call this yourself; use the
        module's load_file() instead. This is here so it can be overridden and/or
        reused by subclasses.
        """
        assistant = dom.documentElement
        if (assistant.namespaceURI != XMLNS or assistant.localName != "DoxyAssist"):
            raise Exception("XML File does not contain DoxyAssist configuration")

        version = "1.0" # Default version to load
        if assistant.hasAttribute("version"):
            version = assistant.getAttribute("version")

        project = cls()

        if version == "1.0":
            project._load_xml_0100(dom)
        else:
            raise Exception("Unsupported DoxyAssist version {0} for project".format(version))
        return project

    def _load_xml_0100(self, dom):
        """
        Load a version 1.0 XML definition as project
        @param dom The XML DOM (DocumentElement) to load
        """
        da = dom.documentElement
        for c in da.childNodes:
            if (c.nodeType != Node.ELEMENT_NODE or c.namespaceURI != XMLNS):
                continue
            if c.localName == "doxygen":
                self._load_doxygen_0100(c)
            elif c.localName == "qtHelp":
                self._load_qthelp_0100(c)
            elif c.localName == "projects":
                self._load_projects_0100(c)

    def _load_doxygen_0100(self, elt):
        """ Load a doxygen configuration block from a 1.0 XML definition """
        self.doxyfile = Doxyfile()
        self.doxyfile.settings_from_xml(elt, XMLNS, "1.0")

    def _load_qthelp_0100(self, elt):
        """ Load the QtHelp configuration from XML """
        collectionFile = elt.getAttribute("collectionFile")
        self.qthelp = QHelpCollection(collectionFile)
        if (elt.hasAttribute("projectFile")):
            self.qthelp.projectFile = elt.getAttribute("projectFile")
        if (elt.hasAttribute("storage")):
            self.qthelp.qch_path_prefix = elt.getAttribute("storage")
        if (elt.hasAttribute("copyAction")):
            self.qthelp.copy_action = elt.getAttribute("copyAction")
        self.qthelp.load_assistant_settings(elt, XMLNS)

    def _load_projects_0100(self, elt):
        """
        Load the projects tag from an XML definition.
        @param elt The container element with project nodes
        """
        for c in elt.childNodes:
            if (c.nodeType != Node.ELEMENT_NODE or c.namespaceURI != XMLNS):
                continue
            if c.localName == "project":
                p = DoxygenProject()
                p.load_xml(c, "1.0", None, self)
                self.projects.append(p)

class DoxygenProject(object):
    """
    A DoxygenProject defines a single project to run.

    The project keeps a local copy of the Doxyfile in memory. Whenever the parent
    or config property is changed, no action is taken to correct the local Doxyfile
    for inheritance.

    If after changing the parent or config of the project the Doxyfile needs to be
    reloaded, call reload_doxyfile(). Note that this will cause any and all custom
    settings to go lost.

    Because of this, it is best to load complete Doxyfile items in the following order:

        1. Set parent (will set config) or config
        2. Call reload_doxyfile() to load the parent's Doxyfile
        3. Load Doxyfile settings
        4. Assign other properties
        5. Load subprojects

    Steps 3 and 4 can be interchanged; as soon as Doxygen is run, all the properties
    will be read, and if necessary (i.e. a property is empty) inherited from the
    parent. As such, some Doxygen settings cannot be overridden by modifying the
    Doxyfile; at least not for the Doxygen runs of the Doxygen Assistant.

    Steps 4 and 5 can for the same reasons also be interchanged, as long as
    subprojects are loaded after loading Doxyfile settings (i.e. step 5 must always
    come after step 3 to achieve expected behavior).

    The config property will be the same for all projects. Whenever it is changed,
    the config property of the parent and all subprojects will also be changed.
    """
    def __init__(self, name = '', version = '', parent = None, config = None):
        """
        Create a new DoxygenProject
        @param name The project name
        @param version The project version
        @param parent The parent project, if any
        @param configuration The main configuration which serves as a base
        """
        self._name = name
        self._version = ''
        self._version_specific = None # Inherit
        self._doxyfile = None
        self._input = []
        self._exclude = []
        self._output = ''
        self._logdir = ''
        self._namespace = ''
        self._parent = parent
        if (config == None and parent != None):
            self._config = parent.config
        else:
            self._config = config
        self.projects = []
        self.reload_doxyfile()
        self.skip = False
        """
        Whether to skip this project in a Doxygen run. Can be used by subclasses
        to exclude a parent project, while still running Doxygen over subprojects.
        Note that this is NOT inherited, unlike many other settings.
        """

    def get_name(self):
        if (self.parent != None and self._name == ''):
            return self.parent.name
        return self._name
    def set_name(self, value):
        self._name = value
    name = property(get_name, set_name)
    """ The name of this project """

    def get_full_name(self):
        name = ''
        if (self.parent != None):
            name = self.parent.full_name
            if (self.parent.version != ''):
                name += ' ' + self.parent.version
            name += ' - '
        name += self.name
        return name
    full_name = property(get_full_name)
    """ The full project name, including parent project name and versions. Note
        that the project version is not included; in many cases Doxygen will take
        it from the PROJECT_NUMBER setting, which will be set to the version. """

    def get_version(self):
        if (self.parent != None and self._version == ''):
            return self.parent.version
        return self._version
    def set_version(self, value):
        self._version = value
    version = property(get_version, set_version)
    """ The current version of this project """

    def get_version_specific(self):
        if (self.parent != None and self._version_specific == None):
            return self.parent.version_specific
        elif (self._version_specific == None):
            return False
        return self._version_specific
    def set_version_specific(self, value):
        self._version_specific = value
    version_specific = property(get_version_specific, set_version_specific)
    """ If the project's documentation should be built in a version-specific manner """

    def get_doxyfile(self):
        return self._doxyfile
    def set_doxyfile(self, value):
        self._doxyfile = value
    doxyfile = property(get_doxyfile, set_doxyfile)
    """ The doxyfile object used for this project. """

    def get_input(self):
        if (self.parent != None and len(self._input) == 0):
            return self.parent.input
        return self._input
    def set_input(self, value):
        self._input = value
    input = property(get_input, set_input)
    """ The list of input directories for this project """

    def get_inputs(self):
        result = list()
        result.extend(self.input)
        for sp in self.projects:
            result.extend(sp.inputs)
        return result
    inputs = property(get_inputs)
    """ All combined input directories of this project and subprojects. This list
        can be used to add to excludes of the parent project. """

    def get_exclude(self):
        if (self.parent != None and len(self._exclude) == 0):
            return self.parent.exclude
        return self._exclude
    def set_exclude(self, value):
        self._exclude = value
    exclude = property(get_exclude, set_exclude)
    """ Files/directories to exclude for this project """

    ## Get all excludes for this project. This will extend the excludes list
    # of the project itself with the inputs of all subprojects, given that these
    # do not reappear in the project's include list.
    def get_excludes(self):
        excl = []
        excl.extend(self.exclude)
        # Massive functional programming... Take all subproject's inputs that are
        # not in our our input setting, and exclude them
        # (x in self.projects[sp].inputs - x in self.input)
        for spi in [ x for sp in self.projects for x in sp.inputs if not x in self.input ]:
            excl.append(spi)
        return excl
    excludes = property(get_excludes)
    """ All files and directories to exclude during this run, including input
        directories of subdirectories """

    def get_output(self):
        if (self.parent != None and self._output == ''):
            return self.parent.output
        return self._output
    def set_output(self, value):
        self._output = value
    output = property(get_output, set_output)
    """ The base output directory """

    def get_full_output(self):
        return path.join(self.output, self.full_namespace)
    full_output = property(get_full_output)
    """ The full output directory, project specific """

    def get_logdir(self):
        if (self.parent != None and self._logdir == ''):
            return self.parent.logdir
        return self._logdir

    def set_logdir(self, value):
        self._logdir = value
    logdir = property(get_logdir, set_logdir)
    """ The directory to write log and error files to """

    def get_namespace(self):
        if (self.parent != None and self._namespace == ''):
            return self.parent.namespace
        return self._namespace.lower()
    def set_namespace(self, value):
        self._namespace = value
    namespace = property(get_namespace, set_namespace)
    """ The base namespace for this project """

    def get_full_namespace(self):
        nsparts = []
        if (self.parent != None):
            nsparts.append(self.parent.full_namespace)

        if (self.parent == None or self.namespace != self.parent.namespace):
            nsparts.append(self.namespace)

        nsparts.append(_norm_pattern.sub('-', self.name))
        if (self.version_specific and self.version != ''):
            nsparts.append(_norm_pattern.sub('-', self.version))

        return '.'.join(nsparts).lower()
    full_namespace = property(get_full_namespace)
    """ The full namespace of this and parent projects combined """

    ## Get parent
    def get_parent(self):
        return self._parent

    # Set parent
    def set_parent(self, value):
        # Make sure we don't make cycles
        p = value
        while (p != None):
            if p == self:
                raise ValueError("Cannot set cyclic project parents")
            p = p.parent
        self._parent = value
    parent = property(get_parent, set_parent)
    """ The parent project, if any """

    def get_config(self):
        """
        Get config
        """
        return self._config
    def set_config(self, value):
        """
        Set main 'config' for entire tree
        """
        if (value != self._config):
            self._config = value
            if (self.parent != None):
                self.parent.config = value
            for sp in self.projects:
                sp.config = value
    config = property(get_config, set_config)
    """ The base configuration of this project """

    def _get_base_filename(self):
        """
        Get the base filename, without extension, to use for custom generated
        files for this project
        """
        filenameparts = []
        if (self.parent != None):
            filenameparts.append(self.parent._get_base_filename())

        filenameparts.append(_norm_dot_pattern.sub('-', self.name))
        if (self.version_specific and self.version != ''):
            filenameparts.append(_norm_dot_pattern.sub('-', self.version))
        return '.'.join(filenameparts)

    def get_chm_filename(self):
        return self._get_base_filename() + '.chm'
    chm_filename = property(get_chm_filename)
    """ The filename to use for *.chm (HTML Help) files """

    def get_qch_filename(self):
        return self._get_base_filename() + '.qch'
    qch_filename = property(get_qch_filename)
    """ The .qch filename for Qt Compressed Help files """

    def get_qhp_filter_name(self):
        filtername = self.full_name
        if (self.version_specific and self.version != ''):
            filtername = filtername + ' ' + self.version if filtername != '' else self.version
        return filtername
    qhp_filter_name = property(get_qhp_filter_name)
    """ The .qhp (Qt Help Project) custom filter to put this compressed help file in """

    def get_qhp_filter_attrs(self):
        attrs = []
        if (self.parent != None):
            attrs.extend(self.parent.qhp_filter_attrs)
        normname = _norm_dot_pattern.sub('-', self.name)
        attrs.append(normname)
        if (self.version_specific and self.version != ''):
            attrs.append(normname + '-' + _norm_dot_pattern.sub('-', self.version))
        return attrs
    qhp_filter_attrs = property(get_qhp_filter_attrs)
    """ Attributes of the custom Qt Compressed Help filter """

    def get_logfile(self):
        """
        Get the full path to the log file to write Doxygen the stdout log to,
        based on the logdir and the project name and version (including parents)
        """
        return path.join(path.abspath(path.expandvars(self.logdir)), self.full_namespace + '.log')
    logfile = property(get_logfile)

    def get_errorlogfile(self):
        """
        Get the full path to the error log file to write Doxygen the stderr log to,
        based on the logdir and the project name and version (including parents)
        """
        return path.join(path.abspath(path.expandvars(self.logdir)), self.full_namespace + '-error.log')
    errorlogfile = property(get_errorlogfile)

    def prepare(self):
        """
        Prepare the project for a doxygen run. This is the function to
        reimplement if last-minute changes need to be applied. Default implementation
        does nothing.
        """
        pass

    def applydoxysettings(self):
        """
        Write the project's settings to the Doxyfile. This will override the
        following settings:

            - PROJECT_NAME
            - PROJECT_VERSION
            - INPUT
            - EXCLUDE
            - OUTPUT_DIRECTORY
            - WARN_LOGFILE
            - DOCSET_BUNDLE_ID
            - CHM_FILE
            - QCH_FILE
            - QHP_NAMESPACE
            - QHP_VIRTUAL_FOLDER
            - QHP_CUST_FILTER_NAME
            - QHP_CUST_FILTER_ATTRS
            - QHP_SECT_FILTER_ATTRS
            - ECLIPSE_DOC_ID
        """
        self.doxyfile.set_setting('PROJECT_NAME', self.full_name)
        self.doxyfile.set_setting('PROJECT_NUMBER', self.version)
        self.doxyfile.set_setting('INPUT', self.input)
        self.doxyfile.set_setting('EXCLUDE', self.excludes)
        self.doxyfile.set_setting('OUTPUT_DIRECTORY', self.full_output)
        self.doxyfile.set_setting('WARN_LOGFILE', '') # We'll redirect stderr
        self.doxyfile.set_setting('DOCSET_BUNDLE_ID', self.full_namespace)
        self.doxyfile.set_setting('CHM_FILE', self.chm_filename)
        self.doxyfile.set_setting('QCH_FILE', self.qch_filename)
        self.doxyfile.set_setting('QHP_NAMESPACE', self.full_namespace)
        self.doxyfile.set_setting('QHP_VIRTUAL_FOLDER', self.full_namespace)
        self.doxyfile.set_setting('QHP_CUST_FILTER_NAME', self.qhp_filter_name)
        self.doxyfile.set_setting('QHP_CUST_FILTER_ATTRS', self.qhp_filter_attrs)
        self.doxyfile.set_setting('QHP_SECT_FILTER_ATTRS', self.qhp_filter_attrs)
        self.doxyfile.set_setting('ECLIPSE_DOC_ID', self.full_namespace)

    def reload_doxyfile(self):
        """
        Reset the doxyfile to a (new deep copy) of the parent's
        doxyfile. If the parent is not set, the main configuration's doxyfile is
        used as base instead. If this is also not set, the doxyfile is cleared
        completely.
        """
        if (self.parent != None):
            self.doxyfile = copy(self.parent.doxyfile)
        elif (self.config != None):
            self.doxyfile = copy(self.config.doxyfile)
        else:
            self.doxyfile = Doxyfile()

    def load_xml(self, element, version="1.0", parent = None, config = None, subprojects = True):
        """
        Load a project from XML data. The element that's given should be an XML
        element configuring the project. This will also recursively load subprojects
        if @p subprojects is True.

        Note that due to inheritance of Doxyfile settings, it is highly recommended
        to supply either the parent parameter (the config will then be inherited from
        the parent), or the config parameter (if the project has no parent itself).
        If neither are given, settings may not be properly initialized.

        If you supply both a parent and config setting, remember that the config
        of the parent (and by that the entire project tree) will be reassigned to
        the new config.

        @param cls The class to load
        @param element The base XML element containing the configuration
        @param version The version of the XML file to load
        @param parent The parent project, if any
        @param config The main configuration, if any
        @param subprojects Whether to load subprojects recursively or not.
        """
        if (version > "1.0"):
            raise ValueError("Unsupported version. Cannot load project settings")

        # Inherit Doxygen configuration now
        self.parent = parent
        if (parent != None):
            self.config = parent.config
        if (config != None):
            self.config = config
        self.reload_doxyfile()

        # The code below _may_ change local Doxygen configuration
        subprojectelts = []
        for c in element.childNodes:
            if (c.nodeType != Node.ELEMENT_NODE or c.namespaceURI != XMLNS):
                continue

            if c.localName == "name":
                self.name = DoxygenProject._element_text(c)

            elif c.localName == "version":
                self.version = DoxygenProject._element_text(c)

            elif c.localName == "versionSpecific":
                self.version_specific = DoxygenProject._element_bool(c)

            elif c.localName == "input":
                # Use private manipulation, otherwise we modify parent
                self._input.append(DoxygenProject._element_text(c))

            elif c.localName == "exclude":
                # Use private manipulation, otherwise we modify parent
                self._exclude.append(DoxygenProject._element_text(c))

            elif c.localName == "output":
                self.output = DoxygenProject._element_text(c)

            elif c.localName == "logDirectory":
                self.logdir = DoxygenProject._element_text(c)

            elif c.localName == "namespace":
                self.namespace = DoxygenProject._element_text(c)

            elif c.localName == "doxygen":
                self.doxyfile.settings_from_xml(c, XMLNS, version)

            elif (c.localName == "project" and subprojects):
                subprojectelts.append(c)

        # Now the doxyfile is fully initialized, load subprojects
        for spe in subprojectelts:
            subproject = self.__class__()
            subproject.load_xml(spe, version, self, self.config, subprojects)
            self.projects.append(subproject)

    @staticmethod
    def _element_text(elt):
        """
        Get the text contents of an XML element
        """
        elt.normalize()
        data = ''
        for c in elt.childNodes:
            if c.nodeType == Node.TEXT_NODE:
                data += c.nodeValue
        return data

    @staticmethod
    def _element_bool(elt):
        value = DoxygenProject._element_text(elt)
        if not (value in ['true', 'false', '1', '0']):
            raise ValueError('Value %s cannot be converted to a boolean' % value)
        return (value in ['true', '1'])

# Import subproject types here; they need the above classes defined
from .doxygendrupalconfiguration import DoxygenDrupalConfiguration

def load_file(filename):
    """
    Load an XML file and construct the DoxygenConfiguration

    The XML file configuration can specify the type of project in the main tag. If
    the type is recognized, the XML configuration will be fed to the project's
    static load_xml() method.
    """
    dom = minidom.parse(filename)

    assistant = dom.documentElement
    if (assistant.namespaceURI != XMLNS or assistant.localName != "DoxyAssist"):
        raise Exception("XML File does not contain DoxyAssist configuration" % version)

    projectType = ''
    if assistant.hasAttribute('type'):
        projectType = assistant.getAttribute('type')
    else:
        projectType = 'generic'

    config = None
    if projectType == 'generic':
        config = DoxygenConfiguration.load_xml(dom)
        config.name = assistant.getAttribute('name') if assistant.hasAttribute('name') else ''
    elif projectType == 'drupal':
        config = DoxygenDrupalConfiguration.load_xml(dom)
        config.name = assistant.getAttribute('name') if assistant.hasAttribute('name') else ''
    else:
        raise ValueError('Invalid project type "%s"' % projectType)
    return config
