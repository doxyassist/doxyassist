
#  DoxyAssist - making modular Doxygen documentation.
#  Copyright (C) 2010-2012 Wouter Haffmans (cheetah@simply-life.net)
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
@file
This module allows loading and modifying doxygen configuration files (Doxyfiles)
by loading them, changing settings and feeding them to Doxygen (using the
doxygenrunner module).
"""

from doxyassist.theme import DoxyAssistTheme
from string import Template
import re
import shlex
import sys
from xml import dom
from xml.dom import Node

class Doxyfile(object):
    """
    A representation of a Doxyfile.

    This also provides functionality to modify settings of the Doxyfile at runtime,
    without changing the original file.

    To retrieve the contents, get the 'contents' property
    """

    # Note: "end-of-line $"s are doubled because of template string use!
    # Break-down of regex:
    # - At beginning of line, start group 'key'
    # - First take white-space characters except if they are an end-of-line character: ((?!$$)\s)*
    # - Fill in setting name (replaced by string templates)
    # - Match further white-space, non-eol chars, followed by = and another set of white-space chars
    # - Start value group with any chars (except end of line)
    # - Match any number of times, a backslash that is followed by a newline character,
    #   and consume that newline character (as white-space) and consume line
    # - Repeat consuming lines if backslash at end of previous line
    # - And at eol that is not preceded by a backslash
    # "$" (or "$$" in the template) is used as generic end-of-line, which I assume
    # includes \r, \n and \r\n (and possibly more in unicode?)
    _setting_pattern = Template(r'^(?P<key>((?!$$)\s)*${setting}((?!$$)\s)*=)((?!$$)\s)*(?P<value>.*(\\(?=$$)\s.*)*)(?<!\\)$$')
    """
    Pattern template for regular expressions; format this with the setting name
    (escaped for regular expressions) to get the pattern for a specific
    Doxygen setting.
    """

    def __init__(self):
        """
        Initialize an empty Doxyfile.

        @see: load_from_string()
        @see: load_from_file()
        """

        self.filename = ''
        """ The original filename of the Doxyfile. """
        self.contents = ''
        """ The current (textual) contents of the Doxyfile. Please use set_setting()
            to modify this, unless you set a complete Doxyfile at once. """
        self.encoding = 'utf-8'
        """ The encoding used when loading the file. Currently only utf-8 is
            properly supported. """

    def setting(self, name):
        """
        Read a setting from a Doxyfile.
        @param name: The name of the setting to retrieve
        @return: the value of the setting as list of string values. If the setting
                 is empty or does not occur
        """
        pattern = Doxyfile._setting_pattern.substitute(setting=re.escape(name))
        match = re.search(pattern, self.contents, re.M)
        if match:
            str_value = match.group('value')
            return [x.strip() for x in self.split_raw_value(str_value) if x.strip() != '']
        return []

    def split_raw_value(self, value):
        """
        Split a setting's value from a Doxyfile into a list of values.
        We can't use shlex.split() properly, because that'll break the use of
        backslashes in paths etc. Very annoying.
        @param value: The values as one string.
        @return A list of values.
        """
        values = []
        escaped = False
        quoted = False
        current = ""

        for c in value:
            isNewline = (c == '\n' or c == '\r')
            isSpace = (c == ' ' or c == '\t' or c == '\n' or c == '\r')

            if c == '"' and not escaped:
                quoted = (not quoted)
            elif c == '\\' and not escaped:
                escaped = True
            elif isSpace and not escaped and not quoted:
                # Item splitting space
                if (len(current) > 0):
                    values.append(current)
                current = ""
            elif isNewline and escaped and not quoted:
                # Newline to continue list (because it's escaped)
                escaped = False
                if (len(current) > 0):
                    values.append(current)
                current = ""
            else:
                if escaped:
                    current += str('\\')
                escaped = False
                current += str(c)

        if (len(current) > 0):
            values.append(current)

        return values;

    def set_setting(self, name, value):
        """
        Change a setting in the doxyfile.

        This replaces the setting in the doxyfile contents if it exists. Otherwise
        it is appended.

        @param name: The name of the setting
        @param value: The value (as list, boolean, integer or string)
        @param encoding: The encoding of the string or list of strings value that
                         is given. Defaults to the doxyfile's encoding.
        @todo Currently only UTF-8 encoding is properly supported for this entire
              module; perhaps test with some different encodings, check conversions
              etc...
        """
        # _setting_pattern = Template(r'^(\s*${setting}\s*=)\s*(.*(\\\n.*)*)(?<!\\)$$')
        string_pattern = self._setting_pattern.substitute(setting=re.escape(name))
        pattern = re.compile(string_pattern, re.M);

        str_value = ''

        if (isinstance(value, list)):
            # Wrap each item in quotes
            value = [ self.escape_value(x) for x in value ]
            value = [ str(x) for x in value ]
            # Concatenate items as string
            str_value = str(" \\\n\t").join(value)
        elif(isinstance(value, bool)):
            str_value = 'YES' if value else 'NO'
        elif(isinstance(value, int)):
            str_value = str(value)
        else:
            # Wrap in quotes
            str_value = self.escape_value(value)

        match = pattern.search(self.contents)

        if match:
            # Escape backslashes, to prevent group matches in value etc (e.g. \1 needs to be \\1 for re)
            self.contents = pattern.sub(match.group('key').replace('\\', '\\\\') + " " + str_value.replace('\\', '\\\\'), self.contents)
        else:
            self.contents += "\n" + name + " = " + str_value + "\n"

    def escape_value(self, value):
        """
        Escape a setting's value. This also wraps it in double quotes, escapes
        backslashes and double quotes. If the value contains an '=' mark, the items
        before and after the = are wrapped in quotes if necessary. Wrapping only
        occurs if there is a space in the value.
        """
        # Always escape backslashes
        values = value.split('=', 1);
        newvalues = list()
        for s in values:
            # Escape " and \ which are not at beginning or end
            s = re.sub(r'(?<=.)(["])(?=.)', r'\\\1', s)
            if (re.search(r'\s', s) != None) and (re.match(r'^".*"$', s) == None):
                s = '"' + s + '"'
            newvalues.append(s)
        return "=".join(newvalues)

    def bool_setting(self, name, default = False):
        """
        Read a boolean setting from a Doxyfile

        Reads a boolean setting. If the setting is not set, the C{default} value is
        returned. If the set value is not a boolean ("YES" or "NO), a TypeError is
        raised.

        @param name: The name of the setting to load
        @param default: The default value to return, if the setting is not set
        @return: The boolean value of the setting in the Doxyfile
        @see: setting()
        """
        values = self.setting(name)
        if not values:
            return default

        value = values[0]
        if (value == "YES" or value == "yes" or value == "1" or value == "true" or value == "TRUE"):
            return True
        if (value == "NO" or value == "no" or value == "0" or value == "false" or value == "FALSE"):
            return False
        raise TypeError('Setting "%s" (value "%s") is not a valid boolean setting.' % (name, value))

    def int_setting(self, name, default = 0, base = 10):
        """
        Read an integer setting from a Doxyfile
        Reads an integer setting. If the setting is not set, the C{default} value is
        returned. If the set value is not an integer, a TypeError is raised.

        @param name: The name of the setting to load
        @param default: The default value to return, if the setting is not set
        @param base: The base to use for conversion
        @return: The integer value of the setting in the Doxyfile
        @see: setting()
        """
        values = self.setting(name)
        if not values:
            return default

        value = values[0]
        return int(value, base)

    def string_setting(self, name, default = ""):
        """
        Read a string setting from a Doxyfile.

        This simply returns the concatenation of the list for the setting (which
        may be a single element), or the default if the setting is not set.

        @param name: The name of the setting to retrieve
        @param default: The default value to return, if the setting is not set
        @return: The string value of the setting.
        @see: setting()
        """
        values = self.setting(name)
        if not values:
            return default
        return "".join(values)

    def settings_from_xml(self, elt, ns = None, version="1.0"):
        """
        Load a doxygen configuration block from a 1.0 XML definition

        @param elt: The XML DOM element containing the configuration, including an
                    optional doxyfile attribute with a filename to load the initial
                    contents from.
        @param ns: The XML namespace the tags have to be in (the DoxyAssist namespace)
        @param version: The XML version definition. If this is unsupported (i.e. newer than
                        the latest known version), an error is thrown.
        """
        if (version > "1.0"):
            raise ValueError("Unsupported version. Cannot load Doxygen settings")

        if (elt.hasAttribute("doxyfile")):
            doxyfile = path.expandvars(elt.getAttribute("doxyfile"))
            if path.exists(doxyfile):
                self.filename = doxyfile
                with open(self.filename, 'r') as f:
                    self.contents = f.read()

        if (elt.hasAttribute("htmltheme")):
            htmltheme = elt.getAttribute("htmltheme")
            theme = DoxyAssistTheme(path.expandvars(htmltheme))
            theme.apply2doxyfile(self)

        # Load settings from tags
        for node in elt.childNodes:
            if (node.nodeType == Node.ELEMENT_NODE and
                (ns == None or node.namespaceURI == ns) and node.localName == "bool"):
                self._load_boolean_setting(node)
            elif (node.nodeType == Node.ELEMENT_NODE and
                  (ns == None or node.namespaceURI == ns) and node.localName == "int"):
                self._load_int_setting(node)
            elif (node.nodeType == Node.ELEMENT_NODE and
                  (ns == None or node.namespaceURI == ns) and node.localName == "string"):
                self._load_string_setting(node)
            elif (node.nodeType == Node.ELEMENT_NODE and
                  (ns == None or node.namespaceURI == ns) and node.localName == "list"):
                self._load_list_setting(node)

    def _load_boolean_setting(self, elt):
        """
        Load a boolean doxygen setting from an XML element

        @type elt: xml.dom.Element
        @param elt: The XML element with the boolean setting
        @return: The boolean value of the settings
        @rtype: bool
        """
        name = elt.getAttribute("name")
        strValue = self._element_text(elt)
        if not (strValue in ['true', 'false', '1', '0']):
            raise ValueError('Value %s for %s cannot be converted to a boolean' % (strValue, name))
        value = strValue in ['true', '1']
        self.set_setting(name, value)

    def _load_int_setting(self, elt):
        """
        Load an integer doxygen setting from an XML element

        @type elt: xml.dom.Element
        @param elt: The XML element with the integer setting
        @return: The integer value of the settings
        @rtype: int
        """
        name = elt.getAttribute("name")
        value = int(self._element_text(elt))
        self.set_setting(name, value)

    def _load_string_setting(self, elt):
        """
        Load a string doxygen setting from an XML element

        @type elt: xml.dom.Element
        @param elt: The XML element with the string setting
        @return: The string value of the settings
        @rtype: str
        """
        name = elt.getAttribute("name")
        value = self._element_text(elt)
        self.set_setting(name, value)

    def _load_list_setting(self, elt):
        """
        Load a list doxygen setting from an XML element

        @type elt: xml.dom.Element
        @param elt: The XML element to get the text contents of.
        @return: A list with str values of all settings
        @rtype: list
        """
        name = elt.getAttribute("name")
        append = (elt.hasAttribute("append") and elt.getAttribute("append") == "true")
        value = self.setting(name) if append else list()
        xmlns = elt.namespaceURI
        for c in elt.childNodes:
            if (c.nodeType == Node.ELEMENT_NODE and c.namespaceURI == xmlns and c.localName == "item"):
                value.append(self._element_text(c))
        self.set_setting(name, value)

    def _element_text(self, elt):
        """
        Get the text contents of an XML element.

        This is the concatenation of all values of text nodes that are direct
        children of the element

        @type elt: xml.dom.Element
        @param elt: The XML element to get the text contents of.
        @return: The text contents of the element
        @rtype: str
        """
        elt.normalize()
        data = ''
        for c in elt.childNodes:
            if c.nodeType == Node.TEXT_NODE:
                data += str(c.nodeValue)
            if c.nodeType == Node.CDATA_SECTION_NODE:
                data += str(c.data)
        return data

def load_from_string(contents):
    """
    Load a doxyfile from a string. The Doxyfile's contents will be initialized
    to the string.

    @param contents: The contents of the Doxyfile
    """
    doxyfile = Doxyfile()
    doxyfile.contents = contents
    return doxyfile

def load_from_file(filename, file_encoding='utf-8'):
    """
    Load a doxyfile from a file. The Doxyfile's contents will be initialized
    to the contents of the file.

    @param filename: The name of the file to load
    @param file_encoding: The encoding of the file (defaults to UTF-8)
    """
    with io.open(filename, mode='r', encoding=file_encoding) as f:
        contents = f.read()
        doxyfile = load_from_string(contents)
        doxyfile.filename = filename
        return doxyfile
