#!/usr/bin/env python

#  DoxyAssist - making modular Doxygen documentation.
#  Copyright (C) 2010-2012 Wouter Haffmans (cheetah@simply-life.net)
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
@file This file contains the main script for DoxyAssist.

DoxyAssist is a tool to generate modular documentation for large projects.
The documentation can be split up in several modules. Support to create special
viewers using the Qt Assistant is also possible.
"""

import sys
from optparse import OptionParser
from doxyassist import doxygenconfiguration as dc
from doxyassist import doxyfile
from doxyassist import doxygenrunner
from doxyassist import qhelp
from sys import stdout
from os import path

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():
    parser = OptionParser(usage="usage: %prog [options] project.xml", version="0.1")
    parser.add_option("--doxygen", dest="doxygen", help="Command to run doxygen")
    parser.add_option("--qcollectiongenerator", dest="qcollectiongenerator", help="Command to run qcollectiongenerator")
    parser.add_option("--htmltheme", dest="htmltheme", help="Override theme to use for html output")
    parser.add_option("--debug", dest="debug", action="store_true", default=False, help="Run in debug mode. This saves generated doxyfiles, amongst other things.")


    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.error("No project file given")

    if options.doxygen != None:
        doxygenrunner.doxygen = options.doxygen

    if options.qcollectiongenerator != None:
        qhelp.qcollectiongenerator = options.qcollectiongenerator

    for f in args:
        config = dc.load_file(f)
        if (options.htmltheme != None):
            config.force_htmltheme = options.htmltheme

        name = config.name if config.name != '' else f
        print("Running doxygen for DoxyAssist configuration {0}:".format(name))
        outputs = config.doxygen(options.debug)

        # Create Qt Collection
        if config.qthelp != None:
            print("Creating Qt Collection for {0}...".format(name))
            stdout.flush()

            qchs = [ o["qch"] for o in outputs if "qch" in o ]
            for qch in qchs:
                config.qthelp.add_file(qch)
            if config.qthelp.generate() == 0:
                print("OK")
                print("Use the following command to open the assistant with the new collection:")
                print("  assistant -collectionFile {0}".format(path.expandvars(config.qthelp.filename)))
            else:
                print("ERROR!")


if __name__ == "__main__":
    sys.exit(main())
