#!/usr/bin/env python3

import unittest

from doxyassist import doxyfile

class DoxyfileTestCase(unittest.TestCase):
    """ Tests the Doxyfile class. """

    def setUp(self):
        self.doxyfile = doxyfile.load_from_file('test/fixtures/test.Doxyfile')
        """ Doxyfile """

    def test_load(self):
        with open('test/fixtures/test.Doxyfile', 'r') as content_file:
            contents = content_file.read()
        self.assertEqual(self.doxyfile.contents, contents)

    def test_string(self):
        self.assertEqual(self.doxyfile.string_setting('STRING'), 'foo')

    def test_string_quoted(self):
        self.assertEqual(self.doxyfile.string_setting('STRING_QUOTED'), 'foo')

    def test_string_quoted_lines(self):
        self.assertEqual(self.doxyfile.string_setting('STRING_QUOTED_LINES'), "foo\\\nbar")

    def test_string_default(self):
        self.assertEqual(self.doxyfile.string_setting('EMPTY', 'foo'), 'foo')
        self.assertEqual(self.doxyfile.string_setting('NONEXISTENT', 'foo'), 'foo')

    def test_bool(self):
        self.assertEqual(self.doxyfile.bool_setting('BOOL'), True)

    def test_bool_quoted(self):
        self.assertEqual(self.doxyfile.bool_setting('BOOL_QUOTED'), True)

    def test_bool_lowercase(self):
        self.assertEqual(self.doxyfile.bool_setting('BOOL_LOWERCASE'), True)

    def test_bool_numeric(self):
        self.assertEqual(self.doxyfile.bool_setting('BOOL_NUMERIC'), True)

    def test_bool_default(self):
        self.assertEqual(self.doxyfile.bool_setting('EMPTY', False), False)
        self.assertEqual(self.doxyfile.bool_setting('EMPTY', True), True)
        self.assertEqual(self.doxyfile.bool_setting('NONEXISTENT', False), False)
        self.assertEqual(self.doxyfile.bool_setting('NONEXISTENT', True), True)

    def test_bool_no(self):
        self.assertEqual(self.doxyfile.bool_setting('BOOL_NO'), False)

    def test_bool_0(self):
        self.assertEqual(self.doxyfile.bool_setting('BOOL_0'), False)

    def test_number(self):
        self.assertEqual(self.doxyfile.int_setting('NUMBER'), 123)

    def test_number_quoted(self):
        self.assertEqual(self.doxyfile.int_setting('NUMBER_QUOTED'), 123)

    def test_number_default(self):
        self.assertEqual(self.doxyfile.int_setting('EMPTY', 123), 123)
        self.assertEqual(self.doxyfile.int_setting('NONEXISTENT', 123), 123)

    def test_list(self):
        self.assertListEqual(self.doxyfile.setting('LIST'), [
            'foo',
            'b\\ar',
            'ch\\"arlie',
            'foo=bar',
            'foo=bar charlie'
        ])
