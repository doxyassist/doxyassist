The files here present sample configuration files for DoxyAssist projects. You
can use these as a base for your project.

Simply make a copy of the example for the type of project you need. Edit it,
especially tweaking input directories and such, so they match your project.
Finally, run "doxyassist.py file.xml" (with the correct filename for file.xml,
of course), to generate the documentation.

Please read the comments in the XML files carefully for further instructions.
If something goes wrong, you can always regenerate the documentation by simply
running doxyassist again.
