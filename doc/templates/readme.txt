These subdirectories contain Doxygen templates. While they are not specifc for
DoxyAssist, they are useful.

You need to adjust the following settings in the Doxyfile to match the filenames
for the template you wish to use. Alternatively, set them in your XML configuration
file, in the following form:

<string name="MY_SETTING">value</string>

See the doc/examples/generic-example.xml file in the DoxyAssist package for
detailed instructions about how you can apply these settings in the configuration
files.

The settings to change are:

HTML_HEADER - the path to the template's header.html
HTML_FOOTER - the path to the template's footer.html
HTML_STYLESHEET - the path to the template's style.css
IMAGE_PATH - the path to the template's images directory