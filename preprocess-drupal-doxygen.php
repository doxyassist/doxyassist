#!/usr/bin/env php
<?php
# preprocess-drupal-doxygen: Preprocessor for Drupal PHP files, to make it suitable for Doxygen.
# Copyright (C) 2010-2012 Wouter Haffmans <cheetah@simply-life.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Abstract preprocessor, which deals with opening and reading files.
 * If inherited, the doProcessLine() function must be reimplemented,
 * which will do the actuall processing of a line.
 */
abstract class Preprocessor {
  private $_filename = '';

  /**
   * Constructor. Prepares the preprocessor for a certain file given
   * by the filename.
   * @param string $filename The filename or URL the preprocessor will run on.
   */
  public function __construct ($filename) {
    $this->_filename = $filename;
  }

  /**
   * Process the file. This will read the contents of the file to
   * be preprocessed, and work its magic.
   *
   * Internally this calls doProcess() which must be implemented
   * by inherited classes, which will do the actual processing.
   *
   * @return string The processed file.
   */
  public function process() {
    $contents = file_get_contents($this->_filename);
    // Convert Mac/Win line breaks to Unix format.
    $contents = str_replace("\r\n", "\n", $contents);
    $contents = str_replace("\r", "\n", $contents);

    return $this->doProcess($contents);
  }

  /**
   * Process a line of the file
   * @param string $contents The contents of the line to process, including newline character
   * @return string The processed line
   */
  protected abstract function doProcess($contents);
}

/**
 * This preprocessor reads a (Drupal) code file, and processes the doxygen documentationsuch that:
 * - Invalid Doxygen commands prefixed with '@' are escaped
 * - The '%' character is always escaped
 * - Backslash commands are escaped
 * - Drupal specific links (api/constants, api/globals) are replaced with Doxygen links
 * - External links are fixed for Doxygen usage
 */
class CodePreprocessor extends Preprocessor {

  public function __construct ($filename) {
    Preprocessor::__construct($filename);
  }

  protected function doProcess($contents) {
    // Beyond Drupal's API module: we also work on blocks started with "/*!"
    $contents = preg_replace_callback('@/\*[\*!](.*?)\*/@s',
                                      array($this, 'processCommentBlock'),
                                      $contents);
    // And those with at least two lines of /// or //!
    $contents = preg_replace_callback('@(//[/!]).*\\n(\\1.*\\n)+@',
                                      array($this, 'processCommentBlock'),
                                      $contents);
    // But also single line "///<" and "//!<" blocks
    $contents = preg_replace_callback('@(//[/!])<.*@',
                                      array($this, 'processCommentBlock'),
                                      $contents);
    // Return processed file contents
    return $contents;
  }

  private function processCommentBlock($matches) {
    $contents = $matches[0];

    $contents = $this->escapeUnknownCommands($contents);
    $contents = $this->makeAnchorLinks($contents, array(
      '/api/constants' => 'globals_enum.html',
//         '/api/globals' => 'globals_vars.html',
    ));
    $contents = $this->replaceLinks($contents, array(
      '/api/globals' => 'globals.php',
    ));
    $contents = $this->replaceExternalLinks($contents);

    return $contents;
  }

  /**
   * Escape all commands not known by Doxygen.
   * @param string $contents The line to escape commands on
   */
  private function escapeUnknownCommands($contents) {
    static $commandsArray = array(
      'a', 'addindex', 'addtogroup', 'anchor', 'arg', 'attention', 'author', 'b', 'brief', 'bug',
      'c', 'callgraph', 'callgraph', 'callergraph', 'category', 'class', 'code', 'cond',
      'copybrief', 'copydetails', 'copydoc', 'date', 'def', 'defgroup', 'deprecated', 'details', 'dir',
      'dontinclude', 'dot', 'dotfile', 'e', 'else', 'elseif', 'em', 'endcode', 'endcond', 'enddot',
      'endhtmlonly', 'endif', 'endlatexonly', 'endlink', 'endmanonly', 'endmsc', 'endverbatim', 'endxmlonly',
      'enum', 'example', 'exception', 'extends', 'file', 'fn', 'headerfile', 'hideinitializer', 'htmlinclude',
      'htmlonly', 'if', 'ifnot', 'image', 'implements', 'include', 'includelineno', 'ingroup', 'internal',
      'invariant', 'interface', 'latexonly', 'li', 'line', 'link', 'mainpage', 'manonly', 'memberof', 'msc',
      'n', 'name', 'namespace', 'nosubgrouping', 'note', 'overload', 'p', 'package', 'page', 'paragraph',
      'param', 'post', 'pre', 'private', 'privatesection', 'property', 'protected', 'protectedsection',
      'public', 'publicsection', 'protocol', 'ref', 'relates', 'relatesalso', 'remarks', 'return', 'retval',
      'sa', 'section', 'see', 'showinitializer', 'since', 'skip', 'skipline', 'struct', 'subpage',
      'subsection', 'subsubsection', 'test', 'throw', 'todo', 'tparam', 'typedef', 'union', 'until', 'var',
      'verbatim', 'verbinclude', 'version', 'warning', 'weakgroup', 'xmlonly', 'xrefitem',
      'annotatedclasslist', 'classhierarchy', 'define', 'functionindex', 'header', 'headerfilelist',
      'inherit', 'l', 'postheader',
    );
    static $backslashCommandsArray = array(
      'addindex', 'addtogroup', 'anchor', 'arg', 'attention', 'author', 'brief', 'bug',
      'callgraph', 'callgraph', 'callergraph', 'category', 'class', 'code', 'cond',
      'copybrief', 'copydetails', 'copydoc', 'date', 'def', 'defgroup', 'deprecated', 'details', 'dir',
      'dontinclude', 'dot', 'dotfile', 'else', 'elseif', 'em', 'endcode', 'endcond', 'enddot',
      'endhtmlonly', 'endif', 'endlatexonly', 'endlink', 'endmanonly', 'endmsc', 'endverbatim', 'endxmlonly',
      'enum', 'example', 'exception', 'extends', 'file', 'fn', 'headerfile', 'hideinitializer', 'htmlinclude',
      'htmlonly', 'if', 'ifnot', 'image', 'implements', 'include', 'includelineno', 'ingroup', 'internal',
      'invariant', 'interface', 'latexonly', 'li', 'line', 'link', 'mainpage', 'manonly', 'memberof', 'msc',
      'name', 'namespace', 'nosubgrouping', 'note', 'overload', 'package', 'page', 'paragraph',
      'param', 'post', 'pre', 'private', 'privatesection', 'property', 'protected', 'protectedsection',
      'public', 'publicsection', 'protocol', 'ref', 'relates', 'relatesalso', 'remarks', 'return', 'retval',
      'sa', 'section', 'see', 'showinitializer', 'since', 'skip', 'skipline', 'struct', 'subpage',
      'subsection', 'subsubsection', 'test', 'throw', 'todo', 'tparam', 'typedef', 'union', 'until', 'var',
      'verbatim', 'verbinclude', 'version', 'warning', 'weakgroup', 'xmlonly', 'xrefitem',
      'annotatedclasslist', 'classhierarchy', 'define', 'functionindex', 'header', 'headerfilelist',
      'inherit', 'postheader',
    );
    static $noWordCommands = array(
      'f[\$\[\]\{\}]', '[\$@\\\\&~<>#%"{}]',
    );
    static $endlineCommands = array(
      '@}',
    );
    static $backslashCommandRegex = NULL;
    static $commandsRegex = NULL;
    static $noWordCommandsRegex = NULL;
    static $endlineCommandRegex = NULL;

    if (!isset($backslashCommandRegex)) {
      $commandsRegex = '/(^|(?<=\W))(?<!\\\\|@)@(?!' . implode('\W|', $commandsArray) . '\W|'
                     . implode('|', $noWordCommands) . ')/';
      $backslashCommandRegex = '/(^|(?<=\W))(?<!\\\\|@)\\\\(?!'
                             . implode('\W|', $backslashCommandsArray) . '\W|'
                             . implode('|', $noWordCommands) . ')/';
      $endlineCommandRegex = '/(' . implode('|', $endlineCommands) . ').*/';
    }

    // First replace all unknown backslash commands that occur before commands
    $contents = preg_replace($backslashCommandRegex, '\\\\\\\\', $contents);
    // And unknown '@' prefixed commands
    $contents = preg_replace($commandsRegex, '\@', $contents);
    // And commands after which the rest of the line should be ignored
    $contents = preg_replace($endlineCommandRegex, '\1', $contents);
    // Escape all unescaped percentage characters
    $contents = preg_replace('/(?<!\\\\|@)%/', '\\\\%', $contents);
    return $contents;
  }

  private function makeAnchorLinks($contents, $links) {
    foreach($links as $original => $new) {
      $re = '/@link ' . preg_quote($original, '/') . '(\/\S*)?\s(.*\S)\s*@endlink/';
      $contents = preg_replace($re, '<a class="el" href="' . addcslashes($new, '\\') . '">\\2</a>', $contents);
    }
    return $contents;
  }

  private function replaceLinks($contents, $links) {
    foreach($links as $original => $new) {
      $re = '/@link ' . preg_quote($original, '/') . '(\/\S*)?\s(.*\S)\s*@endlink/';
      $contents = preg_replace($re, '@link ' . addcslashes($new, '\\') . ' \\2 @endlink', $contents);
    }
    return $contents;
  }

  private function replaceExternalLinks($contents) {
    # Fix external links for any protocol
    return preg_replace('/@link (([a-zA-Z]+:\/\/|mailto\:)\S*)\s+(.*\S)\s*@endlink/',
                 '<a class="el" href="\\1">\\3</a>', $contents);
  }
}

/**
 * Simple preprocessor which makes a HTML file available for use in Doxygen.
 * The HTML file will be included in the Doxygen documentation, and can be linked
 * to using its base filename.
 */
class HtmlPreprocessor extends Preprocessor {
  private $_basename = ''; /**< The basename of the filename */

  public function __construct ($filename) {
    Preprocessor::__construct($filename);
    $this->_basename = basename($filename);
  }

  protected function doProcess($contents) {
    // Find title in the first listed tag that's in the file
    $title = NULL;
    $titleTags = array('title', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6');
    foreach($titleTags as $tag) {
      $titleMatch = array();
      if (preg_match('@<' . $tag . '(\s.*?)?>\s*(\w.*?)\s*</' . $tag . '\s*?>@is', $contents, $titleMatch)) {
        $title = str_replace("\n", " ", $titleMatch[2]); // Make sure it's all on one line
        break;
      }
    }

    if (!isset($title)) {
      $title = $this->_basename;
    }

    // Get <body> of HTML, if present (otherwise use whole document)
    $bodyMatch = array();
    if (preg_match('!<body(\s.*?)?>(.*)</body(\s.*?)?>!is', $contents, $bodyMatch)) {
      $contents = $bodyMatch[2];
    }

    $contents = str_replace("\n", "\n * ", $contents);
    $contents = preg_replace('!([@\\\\])endhtmlonly!', '\\1<span>endhtmlonly</span>', $contents);
    $contents = $this->replaceLinks($contents, array(
      'http://api.drupal.org/api/file/developer/topics/forms_api.html' => 'forms_api.html',
      'http://api.drupal.org/api/file/developer/topics/forms_api_reference.html' => 'forms_api_reference.html',
      'http://api.drupal.org/api/file/developer/topics/javascript_startup_guide.html' => 'javascript_startup_guide.html',
    ));

    $contents = "/**\n"
              // Clone page's documentation in File listing
              . " * @file\n"
              . " * @brief @link {$this->_basename} {$title} @endlink\n"
              . " * \n"
              . " * This file contains a special documentation topic: @link {$this->_basename} {$title} @endlink.\n"
              . " */\n"
              // Create custom page
              . "/**\n"
              . " * @page {$this->_basename} {$title}\n"
              . " * \n"
              . " * @htmlonly\n"
              . " * " . $contents . "\n"
              . " * @endhtmlonly\n"
              . " */\n";
    return $contents;
  }

  private function replaceLinks($contents, $links) {
    foreach($links as $original => $new) {
      $re = '/(<a\s+[^>]*)href=(["\'])' . preg_quote($original, '/') . '(\/\S*)?\\2/si';
      $contents = preg_replace($re, '\\1href=\\2' . addcslashes($new, '\\') . '\\2', $contents);
    }
    return $contents;
  }
}

// Default to processing stdin if no arguments are given
if ($argc == 1) {
  $argc = 2;
  $argv[1] = '-';
}

// Process all files in argument list
for ($i = 1; $i < $argc; ++$i) {
  $filename = $argv[$i];

  if ($filename == "-") {
    $filename = "php://stdin";
  }

  // Find out type of file (based on filename)
  $processor = NULL;
  $info = pathinfo($filename);
  if (!empty($info['extension']) && in_array($info['extension'], array('html', 'htm', 'xhtml'))) {
    $processor = new HtmlPreprocessor($filename);
  }
  else {
    $processor = new CodePreprocessor($filename);
  }

  // Process file
  print $processor->process();
}
